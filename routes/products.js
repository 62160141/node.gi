const express = require('express')
const router = express.Router()
const products = [
  { id: 1, name: 'IPad gen1 64G wifi', price: 11000.0 },
  { id: 2, name: 'IPad gen2 64G wifi', price: 12000.0 },
  { id: 3, name: 'IPad gen3 64G wifi', price: 13000.0 },
  { id: 4, name: 'IPad gen4 64G wifi', price: 14000.0 },
  { id: 5, name: 'IPad gen5 64G wifi', price: 15000.0 },
  { id: 6, name: 'IPad gen6 64G wifi', price: 16000.0 },
  { id: 7, name: 'IPad gen7 64G wifi', price: 17000.0 },
  { id: 8, name: 'IPad gen8 64G wifi', price: 18000.0 },
  { id: 9, name: 'IPad gen9 64G wifi', price: 19000.0 },
  { id: 10, name: 'IPad gen10 64G wifi', price: 20000.0 }
]
let lastID = 11
const getProducts = function (req, res, next) {
  res.json(products)
}
const getProduct = function (req, res, next) {
  const index = products.findIndex(function (item) {
    return item.id === parseInt(req.params.id)
  })
  if (index >= 0) {
    res.json(products[index])
  } else {
    res.status(404).json({
      code: 404,
      msg: 'No product id ' + req.params.id
    })
  }
}

const addProducts = function (req, res, next) {
  console.log(req.body)
  const newProduct = {
    id: lastID,
    name: req.body.name,
    price: parseFloat(req.body.price)
  }
  products.push(newProduct)
  lastID++
  res.status(201).json(newProduct)
}
const updateProduct = function (req, res, next) {
  const productID = parseInt(req.params.id)
  const product = {
    id: productID,
    name: req.body.name,
    price: parseFloat(req.body.price)
  }
  const index = products.findIndex(function (item) {
    return item.id === productID
  })
  if (index >= 0) {
    products[index] = product
    res.json(products[index])
  } else {
    res.status(404).json({
      code: 404,
      msg: 'No product id ' + req.params.id
    })
  }
}
const deleteProduct = function (req, res, next) {
  const productid = parseInt(req.params.id)
  const index = products.findIndex(function (item) {
    console.log(productid)
    return item.id === productid
  })
  if (index >= 0) {
    products.splice(index, 1)
    res.status(204).send()
  } else {
    res.status(404).json({
      code: 404,
      msg: 'No product id ' + req.params.id
    })
  }
}
router.get('/', getProducts)
router.get('/:id', getProduct)
router.post('/', addProducts)
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)

module.exports = router
